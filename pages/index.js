import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import styles from "./index.module.css"

const oAuthUrl =(redirect_uri) =>  `https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Ffitness.activity.read%20profile%20email%20openid&state=${redirect_uri}&response_type=code&client_id=276549235632-u7me77jancd4lu7mhs9boh9p4nklh4uv.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Fstaging-api-consumer-v2.rippl.club%2Fapi%2Fv2%2Foauth%2FgetAuthData`


export default function Home() {

  const router = useRouter()

  const GoogleSignin = async() => {
    let features = 'menubar=yes,location=yes,resizable=no,scrollbars=yes,status=no';
    let url = oAuthUrl(encodeURI(JSON.stringify({callbackUrl:window.location.href+"auth"})) );
    let other = window.open(url, '_blank', features);
    if(typeof window != undefined){
        window.addEventListener("message", (event) => {
            if(event.data.target === "oauthSetup"){
                if(event.data){
                    other.close()
                }
                let {access_token,refresh_token} = event.data
                localStorage.setItem("oauth_rt",refresh_token)
                localStorage.setItem("oauth_at",access_token)
                window.removeEventListener("message", this, false)  
                router.push('/steps')     
            }
        },true)
      }

}
  return (
    <div className={styles.container}>
      <div onClick={()=>GoogleSignin()} className={styles.google_button}>
        <img src="./google-icon.gif" className={styles.google}></img>
        <div>Signin with google</div>
        </div>
    </div>
  )
}
