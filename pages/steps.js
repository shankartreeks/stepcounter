import { useEffect, useState } from "react"
import axios from "axios";
import styles from "./steps.module.css"
import { useRouter } from 'next/router'



export default function Steps() {
    const [Steps, setSteps] = useState(0)
    const [Stream, setStream] = useState(true)
    const [Loading, setLoading] = useState(true)
    const router = useRouter()

    class stepservices {
      static async getUserStepCount(access_token,start_time) {
          try {
  
            const dataSources = await axios({
                methos:"GET",
                headers:{
                    authorization: "Bearer " + access_token
                },
                "Content-Type": "application/json",
                url:"https://www.googleapis.com/fitness/v1/users/me/dataSources"
            })
  
            let dataStream = dataSources.data.dataSource.find((i)=> i.dataStreamName === "estimated_steps")
            if(dataStream){
              const steps = await axios({
                  method: "POST",
                  headers:{
                      authorization: "Bearer " + access_token
                  },
                  "Content-Type": "application/json",
                  url:"https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate",
                  data:{
                      aggregateBy:[
                          {
                              dataTypeName:"com.google.step_count.delta",
                              dataSourceId:"derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
                          }
                      ],
                      bucketByTime: { "durationMillis": 86400000 },
                      startTimeMillis:new Date(start_time).getTime(),
                      endTimeMillis:new Date().getTime()
                  }
              })  
              let stepsarr = steps.data.bucket.map(i =>{
                  return i?.dataset[0]?.point[0]?.value[0]?.intVal || 0
              })
              let totalsteps = stepsarr.reduce((a, b) => a + b)
              
              return {steps:totalsteps,datastream:true}
  
            }else{
              return {steps:0,datastream:false}
            }
  
          } catch (error) {
              console.log(error)
              router.push("/")
            throw error
          }
        }
  }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(async() => {
        let date = new Date()
        let starttime = new Date(`${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()}`).getTime()
        console.log(date.getMonth())
        let access_token = localStorage.getItem("oauth_at")
        let steps = await stepservices.getUserStepCount(access_token,starttime)
        console.log(steps)
        setStream(steps?.datastream)
        setSteps(steps?.steps)
        setLoading(false)
        
    }, [])
    


  return (
    <div className={styles.container}>
      <div>
      {Loading ? <div>Fetching steps</div> : <div>{Stream ? 
      <div className={styles.step_text}>
              <img src="./walking-orange.gif" className={styles.step_image}></img>
              <div>Total Steps Today: {Steps} </div> 
        </div> 
        : <div>This account is not associated with gfit.</div>}</div>}
      </div>
    </div>
  )
}
