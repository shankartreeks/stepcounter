import { useEffect } from 'react'
import { useRouter } from 'next/router'

export default function Auth() {

const router = useRouter()


useEffect(() => {
    if(router.isReady){
        let {access_token,refresh_token,user_data} = router.query
        let messageObject = {access_token,refresh_token,user_data,target:"oauthSetup"}
        window.opener.postMessage(messageObject, window.opener.origin);
    }
}, [router.isReady])


  return (
    <div></div>
  )
}
